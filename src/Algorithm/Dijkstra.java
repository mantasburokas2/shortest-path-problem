package Algorithm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Dijkstra
{
	private List<Edge> edges;

	private Set<Vertex> visitedNodes;
	private Set<Vertex> unVisitedNodes;
	private Map<Vertex, Double> weights;
	private Map<Vertex, Vertex> routes;

	public Dijkstra(Graph graph)
	{
		this.edges = graph.GetEdges();

		visitedNodes = new HashSet<Vertex>();
		unVisitedNodes = new HashSet<Vertex>();
		weights = new HashMap<Vertex, Double>();
		routes = new HashMap<Vertex, Vertex>();
	}

	public void BeginDijkstra(Vertex vertex)
	{
		unVisitedNodes.add(vertex);
		weights.put(vertex, 0.0);

		while (unVisitedNodes.size() > 0)
		{
			Vertex nearestNode = GetNearestNode(unVisitedNodes);
			visitedNodes.add(nearestNode);
			unVisitedNodes.remove(nearestNode);
			FindNextNodes(nearestNode);
		}
	}

	public void FindNextNodes(Vertex vertex)
	{
		List<Vertex> neighbours = FindNeighbours(vertex);
		double weight;

		for (Vertex neighbour : neighbours)
		{
			if (CompareWeights(neighbour, vertex))
			{
				weight = GetLightestWeight(vertex)
						+ GetWeight(vertex, neighbour);
				weights.put(neighbour, weight);
				unVisitedNodes.add(neighbour);
				routes.put(neighbour, vertex);
			}
		}

	}

	public boolean CompareWeights(Vertex neighbour, Vertex vertex)
	{
		if (GetLightestWeight(neighbour) > (GetLightestWeight(vertex) + GetWeight(
				vertex, neighbour)))
		{
			return true;
		}

		return false;
	}

	public double GetWeight(Vertex vertex, Vertex neighbour)
	{
		for (Edge edge : edges)
		{
			if ((edge.GetStartNode().equals(vertex))
					&& (edge.GetEndNode().equals(neighbour)))
			{
				return edge.GetWeight();
			}
		}

		return -1;
	}

	public List<Vertex> FindNeighbours(Vertex vertex)
	{
		List<Vertex> neighbours = new ArrayList<Vertex>();

		for (Edge edge : edges)
		{
			if (edge.GetStartNode().equals(vertex))
			{
				if (!visitedNodes.contains(edge.GetEndNode()))
				{
					neighbours.add(edge.GetEndNode());
				}
			}
		}

		return neighbours;
	}

	public Vertex GetNearestNode(Set<Vertex> unVisitedNodes)
	{
		Vertex nearest = null;

		for (Vertex vertex : unVisitedNodes)
		{
			if (nearest == null)
			{
				nearest = vertex;
			}
			else
			{
				if (GetLightestWeight(nearest) > GetLightestWeight(vertex))
				{
					nearest = vertex;
				}
			}
		}
		return nearest;
	}

	public double GetLightestWeight(Vertex vertex)
	{
		if (weights.containsKey(vertex))
		{
			return weights.get(vertex);
		}
		else
		{
			return Double.MAX_VALUE;
		}
	}

	public void PrintShortestPath(Vertex vertex)
	{
		String lastStop = vertex.GetName();
		Vertex node = routes.get(vertex);
		List<String> route = new ArrayList<String>();

		if (node == null)
		{
			System.out
					.println("Path was not found by given start/end vertexes");
			return;
		}

		while (node != null)
		{
			route.add(node.GetName());
			node = routes.get(node);
		}

		System.out.print(route.get(route.size() - 1) + " --> ");
		for (int i = route.size() - 2; i > -1; i--)
		{
			System.out.print(route.get(i) + " --> ");
		}
		System.out.print(lastStop);
		System.out.print(" (" + weights.get(vertex) + ") Lt");

		System.out.println("\nEnd.");
	}
}
