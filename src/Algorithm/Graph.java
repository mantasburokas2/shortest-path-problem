package Algorithm;

import java.util.List;
import java.util.Set;

public class Graph
{
	private List<Edge> edges;
	private Set<Vertex> vertexes;

	public Graph(List<Edge> edges, Set<Vertex> vertexes)
	{
		this.edges = edges;
		this.vertexes = vertexes;
	}

	public List<Edge> GetEdges()
	{
		return edges;
	}

	public Set<Vertex> GetVertexes()
	{
		return vertexes;
	}

}
