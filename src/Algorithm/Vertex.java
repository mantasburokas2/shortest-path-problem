package Algorithm;

public class Vertex
{
	private String name;

	public Vertex(String name)
	{
		this.name = name;
	}

	public String GetName()
	{
		return name;
	}

	@Override
	public int hashCode()
	{
		int hash = 5;
		hash = 89 * hash + (this.name != null ? this.name.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;

		Vertex other = (Vertex) obj;

		if (name == null)
		{
			if (other.name != null)
				return false;
		}
		else if (!name.equals(other.name))
			return false;
		return true;
	}

	public String toString()
	{
		return name;
	}
}
