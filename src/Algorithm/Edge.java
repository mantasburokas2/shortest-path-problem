package Algorithm;

public class Edge
{
	private Vertex startNode;
	private Vertex endNode;
	private double weight;

	public Edge(Vertex startNode, Vertex endNode, double weight)
	{
		this.startNode = startNode;
		this.endNode = endNode;
		this.weight = weight;
	}

	public Vertex GetStartNode()
	{
		return startNode;
	}

	public Vertex GetEndNode()
	{
		return endNode;
	}

	public double GetWeight()
	{
		return weight;
	}
}
