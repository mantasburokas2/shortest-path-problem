package Data;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import Algorithm.Dijkstra;
import Algorithm.Edge;
import Algorithm.Graph;
import Algorithm.Vertex;

public class FlightsRepository
{
	private static List<Flight> availableFlights;

	private static String departure;
	private static String destination;

	public FlightsRepository()
	{
		availableFlights = new ArrayList<Flight>();
	}

	public void Read()
	{
		try
		{
			File file = new File("Flights.txt");

			Scanner sc = new Scanner(file);

			String line = sc.nextLine();
			SetDepartureDestination(line);

			while (sc.hasNextLine())
			{
				line = sc.nextLine();

				if (line != null && !line.isEmpty())
				{
					availableFlights.add(SetFlights(line));
				}
			}

			sc.close();
		}
		catch (FileNotFoundException e)
		{
			System.out.println("File not found!");
			e.printStackTrace();
		}
	}

	public void SetDepartureDestination(String line)
	{
		String start = "";
		String end = "";
		
		try
		{
			start = line.substring(0, line.indexOf('-'));
			end = line.substring(start.length() + 2, line.length());
		}
		catch (IndexOutOfBoundsException e)
		{
			System.out.println("Something wrong with data in the file!");
			e.printStackTrace();
		}
		
		departure = start.replaceAll("\\s", "");
		destination = end.replaceAll("\\s", "");
	}

	public Flight SetFlights(String line)
	{
		String start = "";
		String end = "";
		String cost = "";
		
		try
		{
		Matcher matcher = Pattern.compile("\\d+").matcher(line);
		matcher.find();

		start = line.substring(0, line.indexOf('-'));
		end = line.substring(start.length() + 2,
				line.indexOf(matcher.group()));
		cost = line.substring(line.indexOf(matcher.group()),
				line.indexOf("Lt", line.indexOf(matcher.group())));
		}
		catch (IndexOutOfBoundsException e){
			System.out.println("Something wrong with data in the file!");
			e.printStackTrace();
		}

		start = start.replaceAll("\\s", "");
		end = end.replaceAll("\\s", "");
		cost = cost.replaceAll("\\s", "");

		Flight flight = new Flight(start, end, Double.parseDouble(cost));

		return flight;
	}

	public void FindTheCheapestRoute()
	{
		List<Vertex> vertexes = new ArrayList<Vertex>();
		List<Edge> edges = new ArrayList<Edge>();
		Dijkstra algorithm;

		for (Flight flights : availableFlights)
		{
			vertexes.add(ConvertCityToVertex(flights.GetArrivalCity()));
			vertexes.add(ConvertCityToVertex(flights.GetDepartureCity()));;

			edges.add(ConvertFlightToEdge(flights));
		}

		Set<Vertex> setOfVertexes = new HashSet<Vertex>(vertexes);

		Graph graph = new Graph(edges, setOfVertexes);
		algorithm = new Dijkstra(graph);

		Vertex startingNode = new Vertex(departure);
		Vertex endNode = new Vertex(destination);

		algorithm.BeginDijkstra(startingNode);
		algorithm.PrintShortestPath(endNode);
	}

	public Vertex ConvertCityToVertex(String city)
	{
		Vertex vertex = new Vertex(city);
		return vertex;
	}

	public Edge ConvertFlightToEdge(Flight flight)
	{
		Vertex startNode = ConvertCityToVertex(flight.GetDepartureCity());
		Vertex endNode = ConvertCityToVertex(flight.GetArrivalCity());

		Edge edge = new Edge(startNode, endNode, flight.GetFlightCost());
		return edge;
	}
}
