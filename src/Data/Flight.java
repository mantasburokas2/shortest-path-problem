package Data;

public class Flight
{
	private String departureCity;
	private String arrivalCity;
	private double flightCost;

	public Flight()
	{
	}

	public Flight(String departureCity, String arrivalCity, double flightCost)
	{
		this.departureCity = departureCity;
		this.arrivalCity = arrivalCity;
		this.flightCost = flightCost;
	}

	public String GetDepartureCity()
	{
		return departureCity;
	}

	public double GetFlightCost()
	{
		return flightCost;
	}

	public String GetArrivalCity()
	{
		return arrivalCity;
	}
}
