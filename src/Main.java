import Data.FlightsRepository;

public class Main
{
	public static void main(String[] args)
	{
		FlightsRepository flights = new FlightsRepository();
		flights.Read();
		flights.FindTheCheapestRoute();
	}
}